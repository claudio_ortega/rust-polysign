/*
    Problem statement:
    given an array of strings
    obtain the set of strings with the max repetition count

    in case of a non empty input, such result is guaranteed to have at least one element,
    see test1()

    in the case of a tie in the max repeated value the result will have more than one element,
    see test2(), test3()
*/

use std::collections::HashMap;
use std::collections::HashSet;

pub fn rank_top(input: Vec<&str>) -> HashSet<&str> {
    let mut result: HashSet<&str> = HashSet::new();

    // create a map with the number of repetitions found per key
    let mut rep_count_map: HashMap<&str, i32> = HashMap::new();

    for next in &input {
        rep_count_map.entry(next).or_insert(0);
        rep_count_map.insert(next, *rep_count_map.get(next).unwrap() + 1);
    }

    // keep track of the entries having the higher count
    let mut current_max_repeat: i32 = 0;
    for entry in rep_count_map {
        if entry.1 > current_max_repeat {
            current_max_repeat = entry.1;
            result.clear();
        }
        if entry.1 == current_max_repeat {
            result.insert(entry.0);
        }
    }

    result
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test0() {
        verify_expectation(vec![], vec![]);
    }

    #[test]
    fn test1() {
        verify_expectation(vec!["A", "A", "B", "C"], vec!["A"]);
    }

    #[test]
    fn test2() {
        verify_expectation(vec!["A", "B"], vec!["A", "B"]);
    }

    #[test]
    fn test3() {
        verify_expectation(vec!["A", "A", "B", "B", "C"], vec!["A", "B"]);
    }

    #[test]
    fn test4() {
        verify_expectation(vec!["A", "B", "C", "C", "A"], vec!["C", "A"]);
    }

    #[test]
    fn test5() {
        verify_expectation(vec!["C", "B", "C", "C", "A"], vec!["C"]);
    }

    fn verify_expectation(in_value: Vec<&str>, expected: Vec<&str>) {
        let a: HashSet<&str> = rank_top(in_value);
        let b: HashSet<&str> = expected.into_iter().collect();
        let diff: HashSet<&&str> = a.symmetric_difference(&b).collect();
        assert_eq!(diff.len(), 0);
    }
}
